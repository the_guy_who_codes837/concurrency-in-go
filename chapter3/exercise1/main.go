package main

import (
	"errors"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"strings"
	"time"
)

func fetchContent(url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		errStr := fmt.Sprintf("Error response returned with status code %d", resp.StatusCode)
		return "", errors.New(errStr)
	}

	body, err := io.ReadAll(resp.Body)

	if err != nil {
		return "", err
	}

	return string(body), nil

}

func countWord(url string, mp map[string]int) {
	body, err := fetchContent(url)
	if err != nil {
		slog.Error(err.Error())
	}

	words := strings.Split(body, " ")

	for _, word := range words {
		// val, _ := mp[word]
		// mp[word] = val + 1
		mp[word]++
	}
}

func main() {
	mp := make(map[string]int)
	for i := 1; i < 4; i++ {
		url := fmt.Sprintf("https://www.rfc-editor.org/rfc/rfc%d.txt", i)
		go countWord(url, mp)
	}

	time.Sleep(10 * time.Second)

	fmt.Println(mp)
}
