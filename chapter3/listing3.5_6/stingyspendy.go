package main

import (
	"fmt"
	"time"
)

type Bank struct {
	amount int
}

func NewBank(amount int) *Bank {
	return &Bank{amount: amount}
}

func (bank *Bank) Stingy() {
	for range 1000000 {
		bank.amount += 10
	}
	fmt.Println("Finished Calculating Stingy ops")
}

func (bank *Bank) Spendy() {
	for range 1000000 {
		bank.amount -= 10
	}
	fmt.Println("Finished Calculating Spendy ops")

}

func main() {
	bank := NewBank(100)

	fmt.Println("Initial Value", bank.amount)

	go bank.Stingy()
	go bank.Spendy()

	time.Sleep(1 * time.Second)

	fmt.Println("Final Value", bank.amount)

}
