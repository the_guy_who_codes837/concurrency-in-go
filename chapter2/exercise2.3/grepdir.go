package main

import (
	"fmt"
	"log/slog"
	"os"
	"strings"
	"time"
)

var logger *slog.Logger = slog.New(
	slog.NewJSONHandler(
		os.Stdout,
		&slog.HandlerOptions{
			AddSource: true,
		},
	),
)

func readFileContent(filename string) (string, error) {
	data, err := os.ReadFile(filename)
	if err != nil {
		return "", fmt.Errorf("Error reading file %s: %v", filename, err)
	}

	return string(data), nil
}

func hasWord(filename, word string) {
	data, err := readFileContent(filename)
	if err != nil {
		logger.Error(err.Error())
		return
	}

	if strings.Contains(data, word) {
		fmt.Printf("%s is present in %s\n", word, filename)
	} else {
		fmt.Printf("%s is not present in %s\n", word, filename)
	}
}

func readDir(dirName string) ([]string, error) {
	files, err := os.ReadDir(dirName)

	if err != nil {
		return nil, fmt.Errorf("Error reading content of directory:%v", err)
	}

	var filenames []string

	for _, file := range files {
		filenames = append(filenames, dirName+"/"+file.Name())
	}

	return filenames, nil
}

func main() {

	args := os.Args[1:]

	if len(args) != 2 {
		logger.Error("Incorrect number of arguments passed.")
		os.Exit(1)
	}

	word := args[0]
	directoryName := args[1]

	filenames, err := readDir(directoryName)

	if err != nil {
		logger.Error(err.Error())
		os.Exit(1)
	}

	for _, filename := range filenames {
		go hasWord(filename, word)
	}

	time.Sleep(1 * time.Second)
}
