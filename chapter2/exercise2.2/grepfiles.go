package main

import (
	"fmt"
	"log/slog"
	"os"
	"strings"
	"time"
)

var logger *slog.Logger = slog.New(
	slog.NewJSONHandler(
		os.Stdout,
		&slog.HandlerOptions{
			AddSource: true,
		},
	),
)

func readFileContent(filename string) (string, error) {
	data, err := os.ReadFile(filename)
	if err != nil {
		return "", fmt.Errorf("Error reading file %s: %v", filename, err)
	}

	return string(data), nil
}

func hasWord(filename, word string) {
	data, err := readFileContent(filename)
	if err != nil {
		logger.Error(err.Error())
		return
	}

	if strings.Contains(data, word) {
		fmt.Printf("%s is present in %s\n", word, filename)
	} else {
		fmt.Printf("%s is not present in %s\n", word, filename)
	}
}

func main() {
	args := os.Args[1:]
	if len(args) < 2 {
		logger.Error("At least 2 arguments are needed")
	}

	word := args[0]

	filenames := args[1:]

	for _, filename := range filenames {
		go hasWord(filename, word)
	}

	time.Sleep(1 * time.Second)
}
