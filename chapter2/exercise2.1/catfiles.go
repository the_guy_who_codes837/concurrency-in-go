package main

import (
	"fmt"
	"log/slog"
	"os"
	"time"
)

var logger *slog.Logger

func readFileContent(filename string) {
	data, err := os.ReadFile(filename)
	if err != nil {
		logger.Error(err.Error())
		os.Exit(1)
	}
	fmt.Printf("Start routine ***************%s***************\n", filename)
	fmt.Println(string(data))
	fmt.Printf("End routine ***************%s***************\n\n\n\n", filename)

}

func main() {
	logger = slog.New(
		slog.NewJSONHandler(
			os.Stdout,
			&slog.HandlerOptions{
				AddSource: true,
			},
		),
	)
	args := os.Args[1:]

	for _, filename := range args {
		go readFileContent(filename)
	}

	time.Sleep(1 * time.Second)
}
