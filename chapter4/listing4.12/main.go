package main

import (
	"fmt"
	"strconv"
	"sync"
	"time"
)

type ReadWriteMutex struct {
	readersCount int
	readersLock  sync.Mutex
	globalLock   sync.Mutex
}

func (rw *ReadWriteMutex) ReadLock() {
	rw.readersLock.Lock()
	rw.readersCount += 1
	if rw.readersCount == 1 {
		rw.globalLock.Lock()
	}
	rw.readersLock.Unlock()
}

func (rw *ReadWriteMutex) WriteLock() {
	rw.globalLock.Lock()
}

func (rw *ReadWriteMutex) ReadUnlock() {
	rw.readersLock.Lock()
	rw.readersCount -= 1
	if rw.readersCount == 0 {
		rw.globalLock.Unlock()
	}
	rw.readersLock.Unlock()
}

func (rw *ReadWriteMutex) WriteUnlock() {
	rw.globalLock.Unlock()
}

func (rw *ReadWriteMutex) TryLock() bool {
	return rw.globalLock.TryLock()
}

func (rw *ReadWriteMutex) TryReadLock() bool {
	acquiredReadLock := rw.readersLock.TryLock()
	if acquiredReadLock {
		rw.readersCount += 1
		if rw.readersCount == 1 {
			acquiredGlobalLock := rw.TryLock()
			if acquiredGlobalLock {
				rw.readersLock.Unlock()
				return true
			} else {
				rw.readersCount -= 1
				rw.readersLock.Unlock()
				return false
			}
		} else {
			rw.readersLock.Unlock()
			return true
		}
	} else {
		return false
	}
}

func matchRecorder(matchEvents *[]string, mutex *ReadWriteMutex) {
	for i := 0; ; i++ {
		mutex.WriteLock()
		*matchEvents = append(*matchEvents, "Match event "+strconv.Itoa(i))
		mutex.WriteUnlock()
		time.Sleep(200 * time.Millisecond)
	}
}

func copyAllEvents(matchEvent *[]string) []string {
	allEvents := make([]string, 0, len(*matchEvent))
	for _, e := range *matchEvent {
		allEvents = append(allEvents, e)
	}
	return allEvents
}

func clientHandler(matchEvents *[]string, mutex *ReadWriteMutex, at time.Time) {
	for i := 0; i < 100; i++ {
		locked := mutex.TryReadLock()
		if !locked {
			fmt.Println("Could not aquire read lock")
			return
		}
		allEvents := copyAllEvents(matchEvents)
		mutex.ReadUnlock()

		timeTaken := time.Since(at)

		fmt.Println(len(allEvents), "events copied in", timeTaken)
	}
}

func main() {
	mutex := ReadWriteMutex{}
	var matchEvents = make([]string, 0, 10000)
	for j := 0; j < 10000; j++ {
		matchEvents = append(matchEvents, "Match event")
	}

	go matchRecorder(&matchEvents, &mutex)

	start := time.Now()
	for j := 0; j < 5000; j++ {
		go clientHandler(&matchEvents, &mutex, start)
	}

	time.Sleep(8 * time.Second)

	fmt.Println("Added ", len(matchEvents)-10000, " elements")
}
