package main

import (
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"strings"
	"sync"
	"time"
)

const AllLetters = "abcdefghijklmnopqrstuvwxyz"

func countLetters(url string, frequency []int, mutex *sync.Mutex, wg *sync.WaitGroup) {
	defer wg.Done()

	resp, err := http.Get(url)
	if err != nil {
		slog.Error(err.Error())
		return
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		slog.Error(fmt.Sprintf("call to %s Returned non 200 status", url))
		return
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		slog.Error(err.Error())
		return
	}

	// Critical section of execution path is locked
	mutex.Lock()
	for _, b := range body {
		c := strings.ToLower(string(b))
		cIndex := strings.Index(AllLetters, c)
		if cIndex >= 0 {
			frequency[cIndex] += 1
		}
	}
	mutex.Unlock()
}

func main() {
	wg := sync.WaitGroup{}

	mutex := sync.Mutex{}

	frequency := make([]int, 26)

	now := time.Now()

	for i := 1000; i < 1030; i++ {
		url := fmt.Sprintf("https://rfc-editor.org/rfc/rfc%d.txt", i)
		wg.Add(1)
		go countLetters(url, frequency, &mutex, &wg)
	}

	wg.Wait()

	mutex.Lock()
	for i, c := range AllLetters {
		fmt.Printf("%c-%d\n", c, frequency[i])
	}
	mutex.Unlock()

	execTime := time.Since(now)

	fmt.Printf("Program took %f seconds\n", execTime.Seconds())
}
