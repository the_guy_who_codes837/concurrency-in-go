package main

import (
	"fmt"
	"sync"
	"time"
)

func countdown(seconds *int, mutex *sync.Mutex) {
	for {
		time.Sleep(1 * time.Second)
		mutex.Lock()
		secondVal := *seconds
		if secondVal <= 0 {
			mutex.Unlock()
			break
		}
		*seconds = secondVal - 1
		mutex.Unlock()
	}
}

func main() {
	count := 5
	mutex := sync.Mutex{}
	go countdown(&count, &mutex)

	for {
		time.Sleep(500 * time.Millisecond)
		mutex.Lock()
		if count <= 0 {
			mutex.Unlock()
			break
		}
		fmt.Println(count)
		mutex.Unlock()
	}
}
