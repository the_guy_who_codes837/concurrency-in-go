package main

import (
	"errors"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"strings"
	"sync"
	"time"
)

func fetchContent(url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		errStr := fmt.Sprintf("Error response returned with status code %d", resp.StatusCode)
		return "", errors.New(errStr)
	}

	body, err := io.ReadAll(resp.Body)

	if err != nil {
		return "", err
	}

	return string(body), nil

}

func countWord(url string, mp map[string]int, mutex *sync.Mutex) {
	body, err := fetchContent(url)
	if err != nil {
		slog.Error(err.Error())
	}

	words := strings.Split(body, " ")

	mutex.Lock()
	for _, word := range words {
		// val, _ := mp[word]
		// mp[word] = val + 1
		mp[word]++
	}
	mutex.Unlock()
}

func main() {
	mp := make(map[string]int)
	mutex := sync.Mutex{}
	for i := 1; i < 4; i++ {
		url := fmt.Sprintf("https://www.rfc-editor.org/rfc/rfc%d.txt", i)
		go countWord(url, mp, &mutex)
	}

	time.Sleep(4 * time.Second)

	mutex.Lock()
	fmt.Println(mp)
	mutex.Unlock()
}
