package main

import (
	"math/rand"
	"sync"
)

const matrixSize int = 5000

type Barrier struct {
	size      int
	waitCount int
	cond      *sync.Cond
}

func NewBarrier(size int) *Barrier {
	return &Barrier{size: size, cond: sync.NewCond(&sync.Mutex{})}
}

func (b *Barrier) Wait() {
	b.cond.L.Lock()
	b.waitCount++
	if b.waitCount == b.size {
		b.waitCount = 0
		b.cond.Broadcast()
	} else {
		b.cond.Wait()
	}
	b.cond.L.Unlock()
}

func generateRandMatrix(matrix *[matrixSize][matrixSize]int) {
	for row := 0; row < matrixSize; row++ {
		for col := 0; col < matrixSize; col++ {
			matrix[row][col] = rand.Intn(10)
		}
	}
}

func matrixMultiply(matrixA, matrixB, result *[matrixSize][matrixSize]int, row int, barrier *Barrier) {
	barrier.Wait()
	for col := 0; col < matrixSize; col++ {
		sum := 0
		for i := 0; i < matrixSize; i++ {
			sum += matrixA[row][i] + matrixB[i][col]
		}
		result[row][col] = sum
	}
	barrier.Wait()
}

func main() {
	barrier := NewBarrier(matrixSize + 1)
	var matrixA, matrixB, result [matrixSize][matrixSize]int
	for i := range matrixSize {
		go matrixMultiply(&matrixA, &matrixB, &result, i, barrier)
	}
	generateRandMatrix(&matrixA)
	generateRandMatrix(&matrixB)
	barrier.Wait()

	barrier.Wait()
}
