package main

import (
	"fmt"
	"os"
	"path/filepath"
	"slices"
	"sync"
)

func fileSearch(dir string, wg *sync.WaitGroup, fileStroe *[]string, mutex *sync.Mutex) {
	files, _ := os.ReadDir(dir)

	for _, file := range files {
		fpath := filepath.Join(dir, file.Name())
		if file.IsDir() {
			wg.Add(1)
			go fileSearch(fpath, wg, fileStroe, mutex)
		} else {
			mutex.Lock()
			*fileStroe = append(*fileStroe, fpath)
			mutex.Unlock()
			fmt.Println(fpath)
		}
	}
	wg.Done()
}

func main() {
	files := []string{}
	mutex := sync.Mutex{}
	wg := sync.WaitGroup{}
	wg.Add(1)

	go fileSearch("commonfiles", &wg, &files, &mutex)

	wg.Wait()

	fmt.Printf("\n\n\n\n\n")

	for _, file := range files {
		fmt.Println(file)
	}

	slices.Sort(files)

	fmt.Printf("\n\n\n\n\n")

	for _, file := range files {
		fmt.Println(file)
	}
}
