package main

import "fmt"

func primerFactorFilter(numbers <-chan int, quit chan<- int) {
	p := <-numbers

	fmt.Println(p)

	var right chan int

	for number := range numbers {
		if number%p != 0 {
			if right == nil {
				right = make(chan int)
				go primerFactorFilter(right, quit)
			}
			right <- number
		}
	}

	if right == nil {
		close(quit)
	} else {
		close(right)
	}
}

func generateNumbers(numbers chan<- int) {
	for i := 2; i < 1000000; i++ {
		numbers <- i
	}

	close(numbers)
}

func main() {
	numbers := make(chan int)

	quit := make(chan int)

	go primerFactorFilter(numbers, quit)
	go generateNumbers(numbers)

	<-quit

}
