package main

import (
	"fmt"
	"io"
	"net/http"
	"strings"
	"sync"
)

func downLoadPage(i int) chan int {
	output := make(chan int)
	go func() {
		defer close(output)
		url := fmt.Sprintf("https://rfc-editor.org/rfc/rfc%d.txt", i)
		fmt.Println("Downloading", url)
		resp, _ := http.Get(url)
		if resp.StatusCode != 200 {
			panic("Server’s error: " + resp.Status)
		}
		bodyBytes, _ := io.ReadAll(resp.Body)
		output <- strings.Count(string(bodyBytes), "\n")
		resp.Body.Close()
	}()
	return output
}

func Fork() []chan int {
	const pagesToDownload = 30
	var res []chan int
	for i := 1000; i < 1000+pagesToDownload; i++ {
		res = append(res, downLoadPage(i))
	}

	return res
}

func Join(input []chan int) chan int {
	out := make(chan int)
	go func() {
		defer close(out)
		wg := sync.WaitGroup{}
		for _, ip := range input {
			wg.Add(1)
			go func(ip chan int) {
				for val := range ip {
					out <- val
				}
				wg.Done()
			}(ip)
		}
		wg.Wait()
	}()
	return out
}

func main() {
	const pagesToDownload = 30
	totalLines := 0

	out := Join(Fork())

	for val := range out {
		totalLines += val
	}

	fmt.Println("Total lines:", totalLines)
}
