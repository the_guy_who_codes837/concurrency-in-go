package main

import (
	"crypto/sha1"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sync"
)

func FHash(filepath string) []byte {
	file, _ := os.Open(filepath)
	defer file.Close()

	sha := sha1.New()

	io.Copy(sha, file)

	return sha.Sum(nil)
}

func main() {
	dir := os.Args[1]

	files, _ := os.ReadDir(dir)

	wg := sync.WaitGroup{}

	for _, file := range files {
		wg.Add(1)
		go func(filename string) {
			filepath := filepath.Join(dir, filename)
			fmt.Printf("%s - %x\n", filepath, FHash(filepath))
			wg.Done()
		}(file.Name())
	}

	wg.Wait()
}
