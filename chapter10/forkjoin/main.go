package main

import (
	"fmt"
	"io/fs"
	"math"
	"os"
	"path/filepath"
	"sync"
)

func deepestNestedBlock(filepath string) int {
	code, _ := os.ReadFile(filepath)
	level := 0
	max := 0

	for _, c := range code {
		if c == '{' {
			level += 1
			max = int(math.Max(float64(max), float64(level)))
		} else if c == '}' {
			level -= 1
		}
	}

	return max
}

func Fork(dir string, output chan<- int, wg *sync.WaitGroup) {
	filepath.Walk(dir, func(path string, info fs.FileInfo, err error) error {
		if !info.IsDir() {
			wg.Add(1)
			go func() {
				output <- deepestNestedBlock(path)
				wg.Done()
			}()
		}
		return nil
	})
}

func Join(input <-chan int) <-chan int {
	output := make(chan int)
	go func() {
		max := 0
		for out := range input {
			max = int(math.Max(float64(max), float64(out)))
		}
		output <- max
	}()
	return output
}

func main() {
	dir := os.Args[1]
	output := make(chan int)
	wg := sync.WaitGroup{}

	Fork(dir, output, &wg)
	maxOut := Join(output)

	wg.Wait()

	close(output)

	fmt.Println(<-maxOut)
}
