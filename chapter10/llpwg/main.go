package main

import (
	"crypto/md5"
	"crypto/sha1"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sync"
)

func FHash(filepath string) []byte {
	file, _ := os.Open(filepath)
	defer file.Close()

	sha := sha1.New()

	io.Copy(sha, file)

	return sha.Sum(nil)
}

func main() {
	dir := os.Args[1]

	files, _ := os.ReadDir(dir)

	hMd5 := md5.New()

	var prev, next *sync.WaitGroup

	for _, file := range files {
		next = &sync.WaitGroup{}
		next.Add(1)
		go func(filename string, prev, next *sync.WaitGroup) {
			filepath := filepath.Join(dir, filename)
			hashOnFile := FHash(filepath)
			if prev != nil {
				prev.Wait()
			}
			hMd5.Write(hashOnFile)
			next.Done()
		}(file.Name(), prev, next)
	}

	next.Wait()
	fmt.Printf("%s - %x\n", dir, hMd5.Sum(nil))
}
