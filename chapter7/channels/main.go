package main

import (
	"fmt"
	"time"
)

func receive(ch chan int) {
	num := 1
	for {
		fmt.Println("Adding")
		ch <- num
		num++

		if num == 40 {
			close(ch)
			break
		}
	}
}

func main() {
	ch := make(chan int, 5)
	go receive(ch)
	for {
		data, more := <-ch
		time.Sleep(2000 * time.Millisecond)
		fmt.Println(data, more)
		if !more {
			break
		}
	}
}
