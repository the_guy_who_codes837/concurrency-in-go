package main

import (
	"fmt"
)

func findFactors(number int) []int {
	var result []int

	for i := 1; i < number; i++ {
		if number%i == 0 {
			result = append(result, i)
		}
	}

	return result
}

func findFactorsAsync(number int, resChannel chan []int) {
	factors := findFactors(number)
	resChannel <- factors
}

func main() {

	ch1 := make(chan []int)
	ch2 := make(chan []int)
	ch3 := make(chan []int)
	ch4 := make(chan []int)
	ch5 := make(chan []int)

	go findFactorsAsync(3419110721, ch1)
	go findFactorsAsync(4033836234, ch2)
	go findFactorsAsync(4033836235, ch3)
	go findFactorsAsync(4033836236, ch4)
	go findFactorsAsync(4033836237, ch5)

	// findFactors(3419110721)
	// findFactors(4033836234)
	// findFactors(4033836235)
	// findFactors(4033836236)
	// findFactors(4033836237)

	fmt.Println(<-ch1)
	fmt.Println(<-ch2)
	fmt.Println(<-ch3)
	fmt.Println(<-ch4)
	fmt.Println(<-ch5)
}
