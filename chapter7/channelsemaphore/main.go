package main

import (
	"container/list"
	"fmt"
	"sync"
	"time"
)

type CountingSemaphore struct {
	size int
	cond *sync.Cond
}

func NewCountingSemaphore(size int) *CountingSemaphore {
	return &CountingSemaphore{size: size, cond: sync.NewCond(&sync.Mutex{})}
}

func (sema *CountingSemaphore) Acquire() {
	sema.cond.L.Lock()
	if sema.size == 0 {
		sema.cond.Wait()
	}
	sema.size--
	sema.cond.L.Unlock()
}

func (sema *CountingSemaphore) Release() {
	sema.cond.L.Lock()
	sema.size += 1
	sema.cond.Signal()
	sema.cond.L.Unlock()
}

type Channel[T any] struct {
	capacitySema *CountingSemaphore
	sizeSema     *CountingSemaphore
	mutex        sync.Mutex
	buffer       *list.List
}

func NewChannel[M any](size int) *Channel[M] {
	return &Channel[M]{
		capacitySema: NewCountingSemaphore(size),
		sizeSema:     NewCountingSemaphore(0),
		mutex:        sync.Mutex{},
		buffer:       list.New(),
	}
}

func (ch *Channel[T]) Send(data T) {
	ch.capacitySema.Acquire()
	ch.mutex.Lock()
	ch.buffer.PushBack(data)
	ch.mutex.Unlock()
	ch.sizeSema.Release()
}

func (ch *Channel[T]) Receive() T {
	var data T
	ch.capacitySema.Release()
	ch.sizeSema.Acquire()
	ch.mutex.Lock()
	data = ch.buffer.Remove(ch.buffer.Front()).(T)
	ch.mutex.Unlock()
	return data
}

func Producer(ch *Channel[int]) {
	for i := 0; ; i++ {
		time.Sleep(1 * time.Second)
		ch.Send(i)
	}
}

func Consumer(ch *Channel[int]) {
	for {
		data := ch.Receive()
		fmt.Println(data)
	}
}

func main() {
	ch := NewChannel[int](0)
	go Producer(ch)
	go Consumer(ch)
	time.Sleep(100 * time.Second)
}
