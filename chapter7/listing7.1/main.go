package main

import (
	"fmt"
	"time"
)

func receiver(messages <-chan int) {
	for msg := range messages {
		fmt.Println("Received ", msg)
		time.Sleep(1 * time.Second)
	}
}

func sender(messages chan<- int) {
	for i := 0; ; i++ {
		messages <- i
	}
}

func main() {
	msgChannel := make(chan int)

	go receiver(msgChannel)
	for i := 0; i < 3; i++ {
		msgChannel <- i
	}

	close(msgChannel)

	fmt.Println("channel closed")

	time.Sleep(5 * time.Second)
}
