package main

import (
	"fmt"
	"math/rand"
	"time"
)

func player() chan string {
	output := make(chan string)
	count := rand.Intn(100)

	move := []string{"UP", "DOWN", "LEFT", "RIGHT"}

	go func() {
		defer close(output)
		for i := 0; i < count; i++ {
			output <- move[rand.Intn(4)]
			d := time.Duration(rand.Intn(200))
			time.Sleep(d * time.Millisecond)
		}
	}()

	return output
}

func main() {
	player1, player2, player3, player4 := player(), player(), player(), player()

	count := 4

	for count > 1 {
		select {
		case move, moreData := <-player1:
			handleMove("player1", &count, move, moreData)
			if !moreData {
				player1 = nil
			}
		case move, moreData := <-player2:
			handleMove("player2", &count, move, moreData)
			if !moreData {
				player2 = nil
			}
		case move, moreData := <-player3:
			handleMove("player3", &count, move, moreData)
			if !moreData {
				player3 = nil
			}
		case move, moreData := <-player4:
			handleMove("player4", &count, move, moreData)
			if !moreData {
				player4 = nil
			}
		}
	}

	fmt.Println("Game finished")
}

func handleMove(player string, count *int, move string, moreData bool) {
	if moreData {
		fmt.Println(player, ": ", move)
	} else {
		*count -= 1
		fmt.Println(player, " left the game.", " Remaining Player: ", *count)
	}
}
