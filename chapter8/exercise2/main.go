package main

import (
	"fmt"
	"math/rand"
	"time"
)

func generateNumbers() chan int {
	output := make(chan int)
	go func() {
		for {
			output <- rand.Intn(10)
			time.Sleep(200 * time.Millisecond)
		}
	}()

	return output
}

func After(duration time.Duration) chan int {
	output := make(chan int)
	go func() {
		time.Sleep(duration)
		close(output)
	}()
	return output
}

func main() {
	numbers := generateNumbers()

	quitAfter := After(5 * time.Second)

	terminated := false

	for !terminated {
		select {
		case number := <-numbers:
			fmt.Println(number)
		case <-quitAfter:
			terminated = true
			fmt.Println("Program terminated")
		}
	}
}
