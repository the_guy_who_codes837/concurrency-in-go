package main

import (
	"fmt"
	"time"
)

func writeEvery(msg string, seconds time.Duration) <-chan string {
	messages := make(chan string)
	go func() {
		for {
			messages <- msg
			time.Sleep(seconds)
		}
	}()
	return messages
}

func main() {
	messageFromA := writeEvery("Tick", 1*time.Second)
	messageFromB := writeEvery("Tock", 3*time.Second)

	for {
		select {
		case msg1 := <-messageFromA:
			fmt.Println(msg1)
		case msg2 := <-messageFromB:
			fmt.Println(msg2)
		default:
			fmt.Println("No messages waiting")
			time.Sleep(1 * time.Second)
		}
	}
}
