package main

import (
	"fmt"
	"math"
	"math/rand"
)

func primesOnly(input <-chan int) <-chan int {
	results := make(chan int)

	go func() {
		for c := range input {
			isPrime := c != 1
			for i := 2; i < int(math.Sqrt(float64(c))); i++ {
				if c%i == 0 {
					isPrime = false
					break
				}
			}
			if isPrime {
				results <- c
			}
		}
	}()

	return results
}

func main() {
	input := make(chan int)
	primes := primesOnly(input)

	for i := 0; i < 100; {
		select {
		case input <- rand.Intn(1000000000) + 1:
		case p := <-primes:
			fmt.Println("Found Prime: ", p)
			i += 1
		}
	}
}
