package main

import (
	"fmt"
	"time"
)

func sendMsgAfter(duration time.Duration) <-chan string {
	messages := make(chan string)
	go func() {
		time.Sleep(duration)
		messages <- "Hello"
	}()
	return messages
}

func main() {
	messages := sendMsgAfter(3 * time.Second)

	select {
	case msg := <-messages:
		fmt.Println("Message Received: ", msg)
	case tNow := <-time.After(4 * time.Second):
		fmt.Println("Timed out. Waited until: ", tNow.Format("15:04:05"))
	}
}
