package main

import (
	"fmt"
	"sync"
	"time"
)

func playerHandler(cond *sync.Cond, playerRemaining *int, playerId int, cancelled *bool) {
	cond.L.Lock()
	if *cancelled {
		cond.L.Unlock()
		return
	}
	fmt.Println(playerId, ": Connected", " playerRemaining = ", *playerRemaining)
	*playerRemaining--
	if *playerRemaining == 0 {
		cond.Broadcast()
	}
	for *playerRemaining > 0 && !*cancelled {
		fmt.Println(playerId, ": Waiting for more players")
		cond.Wait()
	}

	cond.L.Unlock()
	fmt.Println("All players connected. Ready player", playerId)
}

func expiryTimer(cond *sync.Cond, cancelled *bool, timeToWait int) {
	time.Sleep(time.Duration(timeToWait) * time.Second)
	cond.L.Lock()
	*cancelled = true
	cond.Broadcast()
	cond.L.Unlock()
}

func main() {
	cond := sync.NewCond(&sync.Mutex{})
	playersInGame := 4
	cancelled := false

	go expiryTimer(cond, &cancelled, 1)
	for playerId := 0; playerId < 4; playerId++ {
		go playerHandler(cond, &playersInGame, playerId, &cancelled)
		time.Sleep(1 * time.Second)
	}
}
