package main

import (
	"fmt"
	"os"
	"sync"
)

func stingy(money *int, cond *sync.Cond, wg *sync.WaitGroup) {
	for i := 0; i < 1000000; i++ {
		cond.L.Lock()
		*money += 10
		cond.Signal()
		cond.L.Unlock()
	}
	fmt.Println("Stingy Done")
	wg.Done()
}

func spendy(money *int, cond *sync.Cond, wg *sync.WaitGroup) {
	for i := 0; i < 200000; i++ {
		cond.L.Lock()
		for *money < 50 {
			cond.Wait()
		}
		*money -= 50
		if *money < 0 {
			fmt.Println("Money is negative!")
			os.Exit(1)
		}
		cond.L.Unlock()
	}
	fmt.Println("Spendy Done")
	wg.Done()
}

func main() {
	money := 100
	wg := sync.WaitGroup{}
	mutex := sync.Mutex{}
	cond := sync.NewCond(&mutex)

	wg.Add(2)

	go stingy(&money, cond, &wg)

	go spendy(&money, cond, &wg)

	wg.Wait()

	mutex.Lock()
	fmt.Println("Money in bank account ", money)
	mutex.Unlock()

}
