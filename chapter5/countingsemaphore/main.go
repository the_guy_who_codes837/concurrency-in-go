package main

import (
	"fmt"
	"sync"
	"time"
)

type Semaphore struct {
	permits int
	cond    *sync.Cond
}

func NewSemaphore(permits int) *Semaphore {
	return &Semaphore{
		permits: permits,
		cond:    sync.NewCond(&sync.Mutex{}),
	}
}

func (rw *Semaphore) Acquire() {
	rw.cond.L.Lock()
	for rw.permits <= 0 {
		rw.cond.Wait()
	}
	rw.permits--
	rw.cond.L.Unlock()
}

func (rw *Semaphore) Release() {
	rw.cond.L.Lock()
	rw.permits++
	rw.cond.Signal()
	rw.cond.L.Unlock()
}

func work(i int, semaphore *Semaphore) {
	semaphore.Acquire()
	fmt.Printf("Task %d executing\n", i)
	time.Sleep(5 * time.Second)
	semaphore.Release()
}

func main() {
	semaphore := NewSemaphore(5)

	for i := 0; i < 50; i++ {
		go work(i, semaphore)
	}

	time.Sleep(20 * time.Second)
}
