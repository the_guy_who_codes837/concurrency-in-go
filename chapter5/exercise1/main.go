package main

import (
	"fmt"
	"sync"
	"time"
)

func stingy(amount *int, cond *sync.Cond) {
	for i := 0; i < 1000000; i++ {
		cond.L.Lock()
		*amount += 10
		if *amount >= 50 {
			cond.Signal()
		}
		cond.L.Unlock()
	}
	fmt.Println("Stingy Done")
}

func spendy(amount *int, cond *sync.Cond) {
	for i := 0; i < 200000; i++ {
		cond.L.Lock()
		for *amount < 50 {
			cond.Wait()
		}
		*amount -= 50

		cond.L.Unlock()
	}
	fmt.Println("Spendy Done")
}

func main() {
	money := 100
	cond := sync.NewCond(&sync.Mutex{})
	go stingy(&money, cond)
	go spendy(&money, cond)

	time.Sleep(2 * time.Second)
	cond.L.Lock()
	fmt.Println("Money in bank account: ", money)
	cond.L.Unlock()
}
