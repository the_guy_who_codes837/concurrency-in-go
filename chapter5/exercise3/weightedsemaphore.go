package main

import (
	"fmt"
	"sync"
	"time"
)

type Semaphore struct {
	permits int
	cursor  int
	cond    *sync.Cond
}

func NewSemaphore(permits int) *Semaphore {
	return &Semaphore{
		permits: permits,
		cursor:  permits,
		cond:    sync.NewCond(&sync.Mutex{}),
	}
}

func (rw *Semaphore) Acquire(permit int) {
	rw.cond.L.Lock()
	for rw.cursor <= 0 {
		rw.cond.Wait()
	}
	if rw.cursor-permit < 0 {
		rw.cursor = 0
	} else {
		rw.cursor -= permit
	}
	rw.cond.L.Unlock()
}

func (rw *Semaphore) Release(permit int) {
	rw.cond.L.Lock()
	if rw.cursor+permit > rw.permits {
		rw.cursor = rw.permits
	} else {
		rw.cursor += permit
	}
	rw.cond.Signal()
	rw.cond.L.Unlock()
}

func work(i int, semaphore *Semaphore) {
	semaphore.Acquire(1)
	fmt.Printf("Task %d executing\n", i)
	time.Sleep(2 * time.Second)
	semaphore.Release(1)
}

func main() {
	semaphore := NewSemaphore(5)

	for i := 0; i < 50; i++ {
		go work(i, semaphore)
	}

	time.Sleep(20 * time.Second)
}
