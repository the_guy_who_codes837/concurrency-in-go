package main

import (
	"fmt"
	"sync"
	"time"
)

type ReaderWriter struct {
	readCounter  int
	writeCounter int
	writeActive  bool
	cond         *sync.Cond
}

func NewReaderWriter() *ReaderWriter {
	return &ReaderWriter{cond: sync.NewCond(&sync.Mutex{})}
}

func (rw *ReaderWriter) RLock() {
	rw.cond.L.Lock()
	for rw.writeCounter > 0 || rw.writeActive {
		rw.cond.Wait()
	}
	rw.readCounter++
	rw.cond.L.Unlock()
}

func (rw *ReaderWriter) RUnlock() {
	rw.cond.L.Lock()
	rw.readCounter--
	if rw.readCounter == 0 {
		rw.cond.Broadcast()
	}
	rw.cond.L.Unlock()
}

func (rw *ReaderWriter) Lock() {
	rw.cond.L.Lock()
	rw.writeCounter++
	for rw.readCounter > 0 || rw.writeActive {
		rw.cond.Wait()
	}
	rw.writeCounter--
	rw.writeActive = true
	rw.cond.L.Unlock()
}

func (rw *ReaderWriter) Unlock() {
	rw.cond.L.Lock()
	rw.writeActive = false
	rw.cond.Broadcast()
	rw.cond.L.Unlock()
}

func main() {
	mutex := NewReaderWriter()
	for i := 0; i < 2; i++ {
		go func() {
			for {
				mutex.RLock()
				time.Sleep(1 * time.Second)
				fmt.Println("Read Done")
				mutex.RUnlock()
			}
		}()
	}

	time.Sleep(1 * time.Second)
	mutex.Lock()
	fmt.Println("Write Finished")
	mutex.Unlock()
}
